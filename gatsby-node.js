const path = require('path');

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;
  const result = await graphql(`
    query {
      allPodcast {
        edges {
          node {
            id
            slug
          }
        }
      }
    }
  `);

  if (result.errors) {
    console.log(result.errors);
    return Promise.reject(result.errors);
  }

  console.log({ createPages: result });
  result.data.allPodcast.edges.forEach(({ node }) => {
    createPage({
      path: `/podcasts/${node.slug}`,
      component: path.resolve(`./src/templates/Podcast.js`),
      context: node,
    });
  });
};
