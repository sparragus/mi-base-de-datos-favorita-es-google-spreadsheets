import { Link } from 'gatsby';
import PropTypes from 'prop-types';
import React from 'react';

const Header = ({ siteTitle }) => (
  <header
    style={{
      background: `#0e0e0e`,
      marginBottom: `1.45rem`,
    }}
  >
    <div className="container py-2 d-flex justify-content-between align-items-center">
      <h1 style={{ margin: 0 }}>
        <Link
          to="/"
          style={{
            color: `white`,
            textDecoration: `none`,
          }}
        >
          {siteTitle}
        </Link>
      </h1>

      <h1>
        <a
          style={{ color: 'white' }}
          href="https://docs.google.com/forms/d/e/1FAIpQLSdH4irx4rAxB8GcPAimUetFRtoUXMh2amXk0-VFIMcanP4AZQ/viewform"
        >
          Sugiere un podcast
        </a>
      </h1>
    </div>
  </header>
);

Header.propTypes = {
  siteTitle: PropTypes.string,
};

Header.defaultProps = {
  siteTitle: ``,
};

export default Header;
