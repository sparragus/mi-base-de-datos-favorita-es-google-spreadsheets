import React from 'react';
import { graphql } from 'gatsby';

import './Podcast.scss';
import Layout from '../components/layout';
import SEO from '../components/seo';

export default function Podcast(props) {
  const {
    data: { podcast },
  } = props;
  return (
    <Layout>
      <SEO title={podcast.title} />
      <h1>
        {podcast.title}{' '}
        <small style={{ color: 'gray' }}>({podcast.region})</small>
      </h1>
      <img src={podcast.image} width={300} height={300} />

      <p>{podcast.description}</p>

      <h2>Episodios</h2>
      <ul className="list-unstyled">
        {podcast.episodes.map(episode => (
          <a key={episode.title} href={episode.link}>
            <li
              className="Podcast__episode"
              style={{
                backgroundImage:
                  episode.itunes.image && `url(${episode.itunes.image})`,
              }}
            >
              <p className="Podcast__episode-title m-0">{episode.title}</p>
            </li>
          </a>
        ))}
      </ul>
    </Layout>
  );
}

export const query = graphql`
  query PodcastPageQuery($slug: String) {
    podcast(slug: { eq: $slug }) {
      id
      slug
      region
      title
      description
      image
      episodes {
        title
        link
        itunes {
          image
        }
      }
    }
  }
`;
