const _ = require('lodash');
const RSSParser = require('rss-parser');

const db = require('./db');
const parser = new RSSParser({
  customFields: {
    item: ['enclosure'],
  },
});

exports.sourceNodes = async (
  { actions, createNodeId, createContentDigest },
  configOptions
) => {
  const { createNode } = actions;

  // Gatsby adds a configOption that's not needed for this plugin, delete it
  delete configOptions.plugins;

  function processPodcast(podcast) {
    const nodeId = createNodeId(`podcast-${podcast.id}`);
    const nodeContent = JSON.stringify(podcast);
    const nodeData = Object.assign({}, podcast, {
      id: nodeId,
      parent: null,
      children: [],
      internal: {
        type: `Podcast`,
        content: nodeContent,
        contentDigest: createContentDigest(podcast),
      },
    });

    return nodeData;
  }

  const data = await db();

  const podcasts = await Promise.all(
    data.podcasts.map(async podcast => {
      if (!podcast.rss) {
        return;
      }
      const rss = await parser.parseURL(podcast.rss);
      const preProcessedPodcast = {
        id: _.uniqueId('podcast'),
        slug: _.kebabCase(rss.title),
        region: podcast.region,
        ..._.pick(rss, ['title', 'description', 'feedurl']),
        image: (rss.image && rss.image.url) || (rss.itunes && rss.itunes.image),
        episodes: rss.items.map(episode => {
          return {
            ...episode,
            link: (episode.enclosure && episode.enclosure.url) || episode.link,
          };
        }),
      };
      const nodeData = processPodcast(preProcessedPodcast);

      return nodeData;
    })
  );

  podcasts.filter(Boolean).forEach(nodeData => {
    createNode(nodeData);
  });
};
