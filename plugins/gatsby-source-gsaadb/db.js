const GoogleSpreadsheet = require('google-spreadsheet');
const db = new GoogleSpreadsheet(process.env.GSAADB_SHEET_ID);

module.exports = function() {
  return new Promise((resolve, reject) => {
    db.useServiceAccountAuth(
      {
        client_email: process.env.GSAADB_CLIENT_EMAIL,
        private_key: process.env.GSAADB_PRIVATE_KEY.replace(/\\n/g, '\n'),
      },
      error => {
        if (error) {
          return reject(error);
        }

        db.getInfo(async (error, data) => {
          if (error) {
            return reject(error);
          }

          const podcastsTable = data.worksheets.find(
            worksheet => worksheet.title === 'podcasts'
          );

          const [podcastsRows] = await Promise.all([getRows(podcastsTable)]);

          resolve({
            podcasts: podcastsRows,
          });
        });
      }
    );
  });
};

function getRows(table) {
  return new Promise((resolve, reject) => {
    table.getRows({}, (error, rows) => {
      if (error) {
        return reject(error);
      }
      resolve(rows);
    });
  });
}
